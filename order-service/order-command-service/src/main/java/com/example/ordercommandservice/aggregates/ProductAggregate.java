package com.example.ordercommandservice.aggregates;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import com.example.coreapi.commands.CreateProductCommand;
import com.example.coreapi.events.ProductCreatedEvent;

@Aggregate
@Slf4j
public class ProductAggregate {
    @AggregateIdentifier
    private String productId;
    private String name;

    public ProductAggregate() {
    }

    @CommandHandler
    public ProductAggregate(CreateProductCommand command) {
        log.info("CreateProductCommand received");
        AggregateLifecycle.apply(new ProductCreatedEvent(
                command.getId(),
                command.getName(),
                command.getQuantityInStock(),
                command.getPrice()
        ));
    }

    @EventSourcingHandler
    public void on(ProductCreatedEvent event) {
        log.info("ProductCreatedEvent occurred");
        this.productId = event.getId();
        this.name = event.getName();
    }
}
