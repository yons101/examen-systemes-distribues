package com.example.ordercommandservice.aggregates;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import com.example.coreapi.commands.CreateOrderCommand;
import com.example.coreapi.events.OrderCreatedEvent;

@Aggregate
@Slf4j
public class OrderAggregate {
    @AggregateIdentifier
    private String orderId;
    private String name;

    public OrderAggregate() {
    }

    @CommandHandler
    public OrderAggregate(CreateOrderCommand command) {
        log.info("CreateOrderCommand received");
        AggregateLifecycle.apply(new OrderCreatedEvent(
                command.getId(),
                command.getName()
        ));
    }

    @EventSourcingHandler
    public void on(OrderCreatedEvent event) {
        log.info("OrderCreatedEvent occurred");
        this.orderId = event.getId();
        this.name = event.getName();
    }
}
