package com.example.ordercommandservice.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventsourcing.eventstore.EventStore;
import com.example.coreapi.commands.CreateOrderCommand;
import com.example.coreapi.dtos.OrderRequestDTO;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/orders/commands")
public class OrderCommandController {
    private CommandGateway commandGateway;
    private EventStore eventStore;


    @PostMapping("/create")
    public CompletableFuture<String> newOrder(@RequestBody OrderRequestDTO request) {
        CompletableFuture<String> response = commandGateway.send(new CreateOrderCommand(
                UUID.randomUUID().toString(),
                request.getName()
        ));
        return response;
    }

    @GetMapping("/eventStore/{orderId}")
    public Stream eventStore(@PathVariable String orderId) {
        return eventStore.readEvents(orderId).asStream();
    }
}
