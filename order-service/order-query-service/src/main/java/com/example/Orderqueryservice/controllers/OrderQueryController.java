package com.example.Orderqueryservice.controllers;

import com.example.Orderqueryservice.entities.Order;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import com.example.coreapi.query.GetAllOrdersQuery;
import com.example.coreapi.query.GetOrderByIdQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/orders/query")
public class OrderQueryController {
    private QueryGateway queryGateway;

    @GetMapping("/all")
    public CompletableFuture<List<Order>> orders() {
        return queryGateway.query(new GetAllOrdersQuery(),
                ResponseTypes.multipleInstancesOf(Order.class));
    }

    @GetMapping("/byId/{id}")
    public CompletableFuture<Order> getOrder(@PathVariable String id) {
        return queryGateway.query(new GetOrderByIdQuery(id),
                ResponseTypes.instanceOf(Order.class));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> exceptionHandler(Exception exception) {
        ResponseEntity<String> responseEntity =
                new ResponseEntity(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        return responseEntity;
    }
}
