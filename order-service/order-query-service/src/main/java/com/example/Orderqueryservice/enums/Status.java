package com.example.Orderqueryservice.enums;

public enum Status {
    CREATED,
    ACTIVATED,
    ABANDONED,
    IN_PREPARATION,
    SHIPPED,
    DELIVERED
}
