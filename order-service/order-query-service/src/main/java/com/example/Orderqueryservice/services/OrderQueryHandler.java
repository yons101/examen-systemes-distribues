package com.example.Orderqueryservice.services;

import com.example.Orderqueryservice.entities.Order;
import com.example.Orderqueryservice.repositories.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.queryhandling.QueryHandler;
import com.example.coreapi.query.GetAllProductsQuery;
import com.example.coreapi.query.GetProductByIdQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class OrderQueryHandler {
    public OrderRepository orderRepository;

    @QueryHandler
    public List<Order> orderList(GetAllProductsQuery query) {
        return orderRepository.findAll();
    }

    @QueryHandler
    public Order orderList(GetProductByIdQuery query) {
        return orderRepository.findById(query.getId())
                .orElseThrow(() -> new RuntimeException("order not fount"));
    }
}
