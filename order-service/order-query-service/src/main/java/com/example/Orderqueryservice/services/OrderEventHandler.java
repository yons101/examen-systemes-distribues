package com.example.Orderqueryservice.services;

import com.example.Orderqueryservice.entities.Order;
import com.example.Orderqueryservice.repositories.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import com.example.coreapi.events.OrderCreatedEvent;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class OrderEventHandler {
    private OrderRepository orderRepository;
    @EventHandler
    public void on(OrderCreatedEvent event){
        log.info("************************");
        log.info("OrderCreatedEvent received");
        Order order =new Order();
        order.setId(event.getId());
        orderRepository.save(order);
    }

}
