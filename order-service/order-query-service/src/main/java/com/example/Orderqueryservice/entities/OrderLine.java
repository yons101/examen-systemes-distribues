package com.example.Orderqueryservice.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderLine {
    @Id
    private String id;
    private int quantity;
    private double unitPrice;
    private double discount;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Order order;
}
