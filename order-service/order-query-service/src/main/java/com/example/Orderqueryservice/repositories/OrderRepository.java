package com.example.Orderqueryservice.repositories;

import com.example.Orderqueryservice.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,String> {
}
