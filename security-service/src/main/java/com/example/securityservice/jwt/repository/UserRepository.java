package com.example.securityservice.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.securityservice.jwt.models.User;

import java.util.Optional;
public interface UserRepository extends JpaRepository<User, Long> {
	 Optional<User> findByUsername(String username);

	  Boolean existsByUsername(String username);

	  Boolean existsByEmail(String email);
}
