package com.example.securityservice.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.securityservice.jwt.models.ERole;
import com.example.securityservice.jwt.models.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);
}
