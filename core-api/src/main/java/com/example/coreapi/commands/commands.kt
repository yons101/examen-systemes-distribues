package com.example.coreapi.commands

import org.axonframework.modelling.command.TargetAggregateIdentifier

abstract class BaseCommand<T>(
    @TargetAggregateIdentifier
    open val id: T
)

/*Customer*/
data class CreateCustomerCommand(
    override val id: String,
    val name: String,
    val email: String
) : BaseCommand<String>(
    id
)

data class UpdateCustomerCommand(
    override val id: String,
    val name: String,
    val email: String
) : BaseCommand<String>(
    id
)


/*Inventory*/
data class CreateProductCommand(
    override val id: String,
    val name: String,
    val price: Double,
    val quantityInStock: Int,
    val status: String,
    val category: String

) : BaseCommand<String>(
    id
)

data class UpdateProductCommand(
    override val id: String,
    val name: String,
    val price: Double,
    val quantityInStock: Int,
    val status: String,
    val category: String
) : BaseCommand<String>(
    id
)

data class CreateCategoryCommand(
    override val id: String,
    val name: String,
    val description: String,

    ) :
    BaseCommand<String>(
        id
    )

data class UpdateCategoryCommand(
    override val id: String,
    val name: String,
    val description: String
) : BaseCommand<String>(
    id
)


data class CreateOrderCommand(
    override val id: String,
    val name: String,

    ) :
    BaseCommand<String>(
        id
    )

data class UpdateOrderCommand(
    override val id: String,
    val name: String,
) : BaseCommand<String>(
    id
)
