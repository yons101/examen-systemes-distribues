package com.example.coreapi.events

abstract class BaseEvent<T>(
    open val id: T
)

data class CustomerCreatedEvent(
    override val id: String,
    val name: String,
    val email: String
) : BaseEvent<String>(
    id
)

data class CustomerUpdatedEvent(
    override val id: String,
    val name: String,
    val email: String
) : BaseEvent<String>(
    id
)

data class ProductCreatedEvent(
    override val id: String,
    val name: String,
    val quantityInStock: Int,
    val price: Double

) : BaseEvent<String>(
    id
)

data class ProductUpdatedEvent(
    override val id: String,
    val name: String,
    val quantityInStock: Int,
    val price: Double
) : BaseEvent<String>(
    id
)


data class CategoryCreatedEvent(
    override val id: String,
    val name: String,
    val email: String
) : BaseEvent<String>(
    id
)

data class CategoryUpdatedEvent(
    override val id: String,
    val name: String,
    val email: String
) : BaseEvent<String>(
    id
)


data class OrderCreatedEvent(
    override val id: String,
    val name: String,
) : BaseEvent<String>(
    id
)

data class OrderUpdatedEvent(
    override val id: String,
    val name: String,
) : BaseEvent<String>(
    id
)




