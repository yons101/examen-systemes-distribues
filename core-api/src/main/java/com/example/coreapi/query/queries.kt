package com.example.coreapi.query

class GetAllCustomersQuery {}
data class GetCustomerByIdQuery(
    val id: String
)

class GetAllProductsQuery {}
data class GetProductByIdQuery(
    val id: String
)

class GetAllCategoriesQuery {}
data class GetCategoryByIdQuery(
    val id: String
)


class GetAllOrdersQuery {}
data class GetOrderByIdQuery(
    val id: String
)

