package com.example.coreapi.dtos

data class CustomerRequestDTO(
    var name: String = "",
    var email: String = ""
)


data class ProductRequestDTO(
    var name: String = "",
    var price: Double = 0.0,
    var quantityInStock: Int = 0,
    var status: String = "",
    var category: String = ""
)

data class CategoryRequestDTO(
    var name: String = "",
    var description: String = ""
)

data class OrderRequestDTO(
    var name: String = "",
    var price: Double = 0.0
)

