package com.example.ordercommandservice.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventsourcing.eventstore.EventStore;
import com.example.coreapi.commands.CreateProductCommand;
import com.example.coreapi.dtos.ProductRequestDTO;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/products/commands")
public class ProductCommandController {
    private CommandGateway commandGateway;
    private EventStore eventStore;


    @PostMapping("/create")
    public CompletableFuture<String> newProduct(@RequestBody ProductRequestDTO request) {
        CompletableFuture<String> response = commandGateway.send(new CreateProductCommand(
                UUID.randomUUID().toString(),
                request.getName(),
                request.getPrice(),
                request.getQuantityInStock(),
                request.getStatus(),
                request.getCategory()
        ));
        return response;
    }

    @GetMapping("/eventStore/{productId}")
    public Stream eventStore(@PathVariable String productId) {
        return eventStore.readEvents(productId).asStream();
    }
}
