package com.example.ordercommandservice.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventsourcing.eventstore.EventStore;
import com.example.coreapi.commands.CreateCategoryCommand;
import com.example.coreapi.dtos.CategoryRequestDTO;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/categories/commands")
public class CategoryCommandController {
    private CommandGateway commandGateway;
    private EventStore eventStore;


    @PostMapping("/create")
    public CompletableFuture<String> newCategory(@RequestBody CategoryRequestDTO request) {
        CompletableFuture<String> response = commandGateway.send(new CreateCategoryCommand(
                UUID.randomUUID().toString(),
                request.getName(),
                request.getDescription()
        ));
        return response;
    }

    @GetMapping("/eventStore/{categoryId}")
    public Stream eventStore(@PathVariable String categoryId) {
        return eventStore.readEvents(categoryId).asStream();
    }
}
