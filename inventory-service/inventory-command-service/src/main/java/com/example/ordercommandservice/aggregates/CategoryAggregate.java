package com.example.ordercommandservice.aggregates;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import com.example.coreapi.commands.CreateCategoryCommand;
import com.example.coreapi.events.CategoryCreatedEvent;

@Aggregate
@Slf4j
public class CategoryAggregate {
    @AggregateIdentifier
    private String categoryId;
    private String name;

    public CategoryAggregate() {
    }

    @CommandHandler
    public CategoryAggregate(CreateCategoryCommand command) {
        log.info("CreateCategoryCommand received");
        AggregateLifecycle.apply(new CategoryCreatedEvent(
                command.getId(),
                command.getName(),
                command.getDescription()
        ));
    }

    @EventSourcingHandler
    public void on(CategoryCreatedEvent event) {
        log.info("CategoryCreatedEvent occurred");
        this.categoryId = event.getId();
        this.name = event.getName();
    }
}
