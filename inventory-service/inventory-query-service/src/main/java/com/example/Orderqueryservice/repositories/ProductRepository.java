package com.example.Orderqueryservice.repositories;

import com.example.Orderqueryservice.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,String> {
}
