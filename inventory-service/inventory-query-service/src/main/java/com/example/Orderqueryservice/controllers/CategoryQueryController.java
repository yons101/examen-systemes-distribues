package com.example.Orderqueryservice.controllers;

import com.example.Orderqueryservice.entities.Category;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import com.example.coreapi.query.GetAllCategoriesQuery;
import com.example.coreapi.query.GetCategoryByIdQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/categories/query")
public class CategoryQueryController {
    private QueryGateway queryGateway;

    @GetMapping("/all")
    public CompletableFuture<List<Category>> categories() {
        return queryGateway.query(new GetAllCategoriesQuery(),
                ResponseTypes.multipleInstancesOf(Category.class));
    }

    @GetMapping("/byId/{id}")
    public CompletableFuture<Category> getCategory(@PathVariable String id) {
        return queryGateway.query(new GetCategoryByIdQuery(id),
                ResponseTypes.instanceOf(Category.class));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> exceptionHandler(Exception exception) {
        ResponseEntity<String> responseEntity =
                new ResponseEntity(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        return responseEntity;
    }
}
