package com.example.Orderqueryservice.services;

import com.example.Orderqueryservice.entities.Product;
import com.example.Orderqueryservice.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import com.example.coreapi.events.ProductCreatedEvent;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class ProductEventHandler {
    private ProductRepository productRepository;
    @EventHandler
    public void on(ProductCreatedEvent event){
        log.info("************************");
        log.info("ProductCreatedEvent received");
        Product product =new Product();
        product.setId(event.getId());
        product.setName(event.getName());
        product.setPrice(event.getPrice());
        product.setQuantityInStock(event.getQuantityInStock());
        productRepository.save(product);
    }

}
