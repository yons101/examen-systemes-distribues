package com.example.Orderqueryservice.enums;

public enum Status {
    AVAILABLE,
    OUT_OF_STOCK,
    PRODUCTION,
    ABANDONMENT
}
