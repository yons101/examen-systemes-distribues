package com.example.Orderqueryservice.repositories;

import com.example.Orderqueryservice.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,String> {
}
