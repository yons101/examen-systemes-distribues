package com.example.Orderqueryservice.services;

import com.example.Orderqueryservice.entities.Product;
import com.example.Orderqueryservice.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.queryhandling.QueryHandler;
import com.example.coreapi.query.GetAllProductsQuery;
import com.example.coreapi.query.GetProductByIdQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ProductQueryHandler {
    public ProductRepository productRepository;

    @QueryHandler
    public List<Product> productList(GetAllProductsQuery query) {
        return productRepository.findAll();
    }

    @QueryHandler
    public Product productList(GetProductByIdQuery query) {
        return productRepository.findById(query.getId())
                .orElseThrow(() -> new RuntimeException("product not fount"));
    }
}
