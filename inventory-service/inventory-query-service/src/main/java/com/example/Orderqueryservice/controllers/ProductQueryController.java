package com.example.Orderqueryservice.controllers;

import com.example.Orderqueryservice.entities.Product;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import com.example.coreapi.query.GetAllProductsQuery;
import com.example.coreapi.query.GetProductByIdQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/products/query")
public class ProductQueryController {
    private QueryGateway queryGateway;

    @GetMapping("/all")
    public CompletableFuture<List<Product>> products(){
        return queryGateway.query(new GetAllProductsQuery(),
                ResponseTypes.multipleInstancesOf(Product.class));
    }

    @GetMapping("/byId/{id}")
    public CompletableFuture<Product> getProduct(@PathVariable String id){
        return queryGateway.query(new GetProductByIdQuery(id),
                ResponseTypes.instanceOf(Product.class));
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> exceptionHandler(Exception exception){
        ResponseEntity<String > responseEntity=
                new ResponseEntity(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        return responseEntity;
    }
}
