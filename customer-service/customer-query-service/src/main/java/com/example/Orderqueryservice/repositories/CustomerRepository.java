package com.example.Orderqueryservice.repositories;

import com.example.Orderqueryservice.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,String> {
}
